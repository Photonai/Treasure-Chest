# Treasure Chest

Capture & reexperience any streaming media you've already Paid for! For DRM enabled content this uses Hw HDMI capture cards.
This is similar to having a VCR in the 1990s to record cable TV!

Treasure Chest has  3 key characteristics that set it apart from traditional pirateing:
1. You can never access or Export the files stored by it! But you can backup & restore your Treasure Chest. Everything stays encrypted!
2. Only one instance per registered UniID can run at a time! Meaning you can't 'clone' your entire machine and give it to a friend!
3. Opening a Treasure automatically starts up Dream Coin Mining! We pays 80% of it back to the Artist or rightsholder! Keeping everyone happier.

# Borrow
You can Loan out any piece of media you've capture, to exactly 1 friend at a time! This is similar to real life borrowing from our childhoods! :joy:



# Secure Architecture
We use Xen to isolate a "Treasure Chest" domain which stores and replays content, from the "Control Panel" domain so you never have access to Chest directly! It Always stays encrypted until only a function to replay content! Thus it's designed to Prevent piracty!

## Required passthrough: when playing 
Treasure Chest **Requires** that you pass through the same card you captured it on, the ENTIRE Time you are playing back media! This prevents "Double Capture". A user would have to have 2 entirely separate capture cards. Not impossible! But we chose to make ti harder for them, not easier! Wihtiut this requirement a user could use the Same Card to "Recapture" the media from another host when they're playing it!
 Getting around

#music, TV, movie, book